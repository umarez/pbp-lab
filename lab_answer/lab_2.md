# Apakah perbedaan antara JSON dan XML?

> JSON : Javascript object notation, sebuah format text yang digunakan untuk membawa sebuah data.

> XML : Markup language dengan aturan tertentu yang digunakan untuk membawa sebuah data.

<h3 style="text-align:center;">Perbedaan keduanya </h3>

|                    JSON                    |                                XML                                 |
| :----------------------------------------: | :----------------------------------------------------------------: |
|       Dibangun dengan Key-value pair       |                        Dibangun dengan tags                        |
|      Direpresentasikan sebagai objek       |                  Representasi dengan markup tags                   |
|        Hanya support UTF-8 encoding        |                     Support berbagai encoding                      |
| Tipe data yang dibawa hanya text dan angka | Membawa tipe data yang lebih bervariasi, seperti gambar, graf, dll |

<br> </br>

# Apakah perbedaan antara HTML dan XML

> Meskipun HTML dan XML mirip secara struktur yang membentuknya, Perbedaan utama yang dimiliki yaitu HTML digunakan untuk menampilkan data sedangkan XML digunakan untuk membawa data.

<br/>
Referensi : <a>https://www.guru99.com/json-vs-xml-difference.html<a/>
