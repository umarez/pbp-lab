from django.db import models

# Create your models here.
class Note(models.Model):
    To = models.CharField(max_length=100)
    Title = models.CharField(max_length=80)
    From = models.CharField(max_length=100)
    Message = models.TextField()

    def __str__(self):
        return self.Title

