from django.shortcuts import render
from .forms import FriendForm
from lab_1.models import Friend
from django.http.response import HttpResponseRedirect
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required(login_url='/admin/login/')
def index(req):
    friend_list = Friend.objects.all()
    res = {'friends' : friend_list}
    return render(req, 'lab3_index.html', res)

@login_required(login_url='/admin/login/')
def add_friend(req):
    form = FriendForm(req.POST or None)
    context = {}

    if form.is_valid():
        form.save()
        return HttpResponseRedirect("/lab-3")

    context['form'] = form
    return render(req, 'lab3_form.html', context)
