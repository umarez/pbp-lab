# Generated by Django 3.2.7 on 2021-10-03 05:41

from django.db import migrations


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('lab_1', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='FriendModel',
            fields=[
            ],
            options={
                'proxy': True,
                'indexes': [],
                'constraints': [],
            },
            bases=('lab_1.friend',),
        ),
    ]
