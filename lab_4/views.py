from django.shortcuts import render
from lab_2.models import Note
from django.http.response import HttpResponseRedirect
from .forms import NoteForm

# Create your views here.
def index(request):
    note = Note.objects.all()
    response = { 'Note' : note }
    return render(request, 'lab4_index.html', response)

def add_note(request):
    form = NoteForm(request.POST or None)
    context = {}

    if form.is_valid():
        form.save()
        return HttpResponseRedirect("/lab-4")

    context['form'] = form
    return render(request, 'lab4_form.html', context)

def note_list(request):
    note = Note.objects.all()
    response = { 'Note' : note }
    return render(request, 'lab4_note_list.html', response)